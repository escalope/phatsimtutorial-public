Tutorial for PHATSIM
--------------------
PHATSIM is a simulator for physical activities. The software for PHAT is disseminated through [http://grasia.fdi.ucm.es/sociaal](http://grasia.fdi.ucm.es/sociaal). 

The tutorial has four exercises:

* ex0. It is a quick introduction. It shows the basic lifecycle of modeling and simulation. Also, tells how to share the simulations
* ex1. It introduces modeling primitives for defining characters and their behaviour. Also, tells how intertwine the behavior of different characters.  
* ex2. This time, the tutorial focuses in how to model the effects of a disease into the behaviour of a character.
* ex3. The example focuses into how to integrate an android device into the simulation. The example shows how to link an app to a device and the device to a particular part of the scenario, characters included.

Inside of each exercise, you will find a pdf with instructions and solutions to the most relevant exercises. The exercise is thought to start from the code stored in the "initialX" folder, where X is 0,1,2,or 3, depending on the exercise. Solutions to the exercise are in the folders "solexXX", where XX is 0A, 0B,1B,1C...